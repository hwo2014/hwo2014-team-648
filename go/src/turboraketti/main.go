package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

const (
	numData int = 5
)

type stepData struct {
	tick int64;
	carpos []carPosition;
	lenDriven float64;
}

type botState struct {
	name string;
	reader *bufio.Reader;
	writer *bufio.Writer;

	init *gameInitData;
	lapsLeft int;
	notFirstCarpos bool;
	turboAvailable bool;
	throttle float64;
	
	index int;
	data [numData]stepData;
}

var bot botState

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func readMsg() (msg baseMsg, err error) {
	reader := bot.reader
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(line), &msg)
	return
}

func writeMsg(msgtype string, data interface{}) (err error) {
	writer := bot.writer
	m := map[string]interface{}{}
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)

	_, err = writer.Write(payload)
	if err != nil {
		return
	}

	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}

	writer.Flush()
	return
}

func sendPing() (err error) {
	err = writeMsg("ping", nil)
	return
}

func handleGameInit(data *gameInitData) {
	bot.init = data
	bot.lapsLeft = data.RaceSession.Laps
}

func data(index int) *stepData {
	canonicalIndex := index % numData
	if index < 0 {
		canonicalIndex += numData
	}
	return &bot.data[canonicalIndex]
}

func upData(carpos []carPosition, tick int64) {
	if !bot.notFirstCarpos {
		bot.index = 0
		data(bot.index).tick = tick
		data(bot.index).carpos = carpos
		bot.notFirstCarpos = true
	}
	bot.index = (bot.index + 1) % numData
	data(bot.index).tick = tick
	data(bot.index).carpos = carpos
}

func upLenDriven() {
	prevLenDriven := data(bot.index - 1).lenDriven
	prevPieceIndex :=
		data(bot.index - 1).carpos[0].PiecePosition.PieceIndex
	curPieceIndex :=
		data(bot.index).carpos[0].PiecePosition.PieceIndex

	// still in the same piece
	if prevPieceIndex == curPieceIndex {
		data(bot.index).lenDriven = prevLenDriven +
			(data(bot.index).carpos[0].PiecePosition.InPieceDistance -
			data(bot.index - 1).carpos[0].PiecePosition.InPieceDistance)
	} else {
		prevPieceLen := bot.init.Race.Track.Pieces[prevPieceIndex].Length
		// assume 1-piece difference
		data(bot.index).lenDriven = prevLenDriven +
			(prevPieceLen -
			data(bot.index - 1).carpos[0].PiecePosition.InPieceDistance) +
			data(bot.index).carpos[0].PiecePosition.InPieceDistance
		
	}
 
}

func speed() float64 {
	diffLenDriven := data(bot.index).lenDriven - data(bot.index - 1).lenDriven
	diffTicks := data(bot.index).tick - data(bot.index - 1).tick
	return diffLenDriven/float64(diffTicks)
}

/*
func speed() (speed float64) {
	track := &bot.init.Race.Track
	totalNumPieces := len(track.Pieces)
	curPieceIndex := bot.carpos[bot.index][0].PiecePosition.PieceIndex
	prevPieceIndex := bot.carpos[(bot.index - 1) % numData][0].
		PiecePosition.PieceIndex
	numPiecesTraveled := ((curPieceIndex + prevPieceIndex) % totalNumPieces)
	var totalPiecesLen float64
	var inPieceDistances float64
	for i := prevPieceIndex; i <= i + numPiecesTraveled; i++ {
		totalPiecesLen += track.Pieces[i % totalNumPieces].Length
	//:=  +
	//	track.Pieces[prevPieceIndex].Length
	}
	
	return lenPieces
}
*/

func upThrottle(throttle float64) (err error) {
	bot.throttle = throttle
	err = writeMsg("throttle", throttle)
	return
}

func step(msg *baseMsg) (err error) {
	// non-game logic/ping replies
	if msg.Msgtype != "gameStart" &&
		msg.Msgtype != "crash" &&
		msg.Msgtype != "turboAvailable" &&
		msg.Msgtype != "carPositions" {
		switch msg.Msgtype {
		case "join":
			log.Printf("joined")
		case "gameInit":
			var data gameInitData
			err = json.Unmarshal(msg.Data, &data)
			handleGameInit(&data)
		case "crash":
			log.Printf("someone crashed")
		case "gameEnd":
			log.Printf("game ended")
			err = writeMsg("tournamentEnd", nil)
			os.Exit(0)
		case "error":
			log.Printf("error: %+v", msg.Data)
		default:
			log.Printf("unknown message: %s: %+v", msg.Msgtype, msg.Data)
		}
		sendPing()
		return
	}

	// game logic replies
	switch msg.Msgtype {
	case "gameStart":
		log.Printf("game started")
		upThrottle(1.0)
	case "crash":
		log.Printf("someone crashed")
		sendPing()
	case "turboAvailable":
		bot.turboAvailable = true
		sendPing()
	case "carPositions":
		var carpos []carPosition
		err = json.Unmarshal(msg.Data, &carpos)
		upData(carpos, msg.GameTick)
		upLenDriven()
		
		upThrottle(0.5)
	}
	return
}

func botLoop(conn net.Conn, name string, key string) (err error) {
	bot = botState{
		reader: bufio.NewReader(conn),
		writer: bufio.NewWriter(conn),
	}

	data := map[string]string{
		"name":name,
		"key":key,
	}
	err = writeMsg("join", data)

	for {
		input, err := readMsg()
		if err != nil {
			log.Fatal(err)
		}

		err = step(&input)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func parseArgs() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func main() {
	host, port, name, key, err := parseArgs()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	err = botLoop(conn, name, key)
}

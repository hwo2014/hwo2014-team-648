package main

import (
	"encoding/json"
)

type baseMsg struct {
	Msgtype string `json:"msgtype"`;
	Data json.RawMessage `json:"data"`;
	GameId string `json:"gameData"`;
	GameTick int64 `json:"gameTick"`;
}

type gameInitData struct {
	Race struct {
		Track struct {
			Id string `json:"id"`;
			Name string `json:"name"`;
			Pieces []struct {
				Length float64 `json:"length"`;
				Angle float64 `json:"angle"`;
				Switch bool `json:"switch"`;
			} `json:"pieces"`;
			Lanes []struct {
				DistanceFromCenter float64 `json:"distanceFromCenter"`;
				Index int `json:"index"`;
			} `json:"lanes"`;
		} `json:"track"`;
	} `json:"race"`;
	Cars []struct {
		Id struct {
			Name string `json:"name"`;
			Color string `json:"color"`;
		} `json:"id"`;
		Dimensions struct {
			Length float64 `json:"length"`;
			Width float64 `json:"width"`;
			GuideFlagPosition float64 `json:"guideFlagPosition"`;
		} `json:"dimensions"`;
	};
	RaceSession struct {
		Laps int `json:"laps"`;
		MaxLapTimeMs float64 `json:"maxLapTimeMs"`;
		QuickRace bool `json:"quickRace"`;
	} `json:"raceSession"`;
}

type carPosition struct {
	Id struct {
		Name string `json:"name"`;
		Color string `json:"color"`;
	} `json:"id"`;
	Angle float64 `json:"angle"`;
	PiecePosition struct {
		PieceIndex int `json:"pieceIndex"`;
		InPieceDistance float64 `json:"inPieceDistance"`;
		Lane struct {
			StartLaneIndex int `json:"startLaneIndex"`;
			EndLaneIndex int `json:"endLaneIndex"`;
		} `json:"lane"`;
	} `json:"piecePosition"`;
	Lap int `json:"lap"`;
}

